import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import  mongoose from "mongoose";
import { Routes } from './routes/Routes';
import config from './config/Config';
import HttpResponse from './interface/HttpResponse';
import https from 'https';
class App {

    public app : express.Application;
    public routePrv: Routes = new Routes();
    // public customerRoutes : CustomerRoutes = new CustomerRoutes();
    public mongoUrl: string = 'mongodb://localhost/CRMdb';

    constructor() {
        this.app = express();
        this.config();
        this.routePrv.route(this.app);
        // this.customerRoutes.route(this.app);
        this.handleResponse(); 
        this.mongoSetup();
    }

    private config() : void {
        this.app.use(helmet());
        this.app.use(cors());
        this.app.use(express.json());
    }

    private handleResponse() : void {
        this.app.use((httpResponse : HttpResponse, req: any, res: any, next : any) => {
            
            res.status(httpResponse.httpStatusCode).json(httpResponse);    
        })
    }

   

    private mongoSetup(): void{
        mongoose.connect(config.mongo.url, config.mongo.options)
    .then((result) => {
        console.log(result);
    })
    .catch((error) => {
        console.log(error);
        // 
    });
        }
        
}

export default new App().app;