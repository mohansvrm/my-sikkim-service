import { Request, Response } from 'express';
import HotelController from '../controller/HotelController';
import DestinationController from '../controller/DestinationController';
import TrekController from '../controller/TrekController';
import TravelAgentController from '../controller/TravelAgentController';
import FeedBackController from '../controller/FeedBackController';
import ExperienceController from '../controller/ExperienceController';
import MasterController from '../controller/MasterController';
import PaymentController from '../controller/PaymentController';
import CartController from '../controller/OrderController';
import UserController from '../controller/UserController';
import PermitController from '../controller/PermitController';
import FileUploadController from '../controller/FileUploadController';
import multer from 'multer';
import multerS3 from 'multer-s3';
import config from '../config/Config';

export class Routes {
 

    public route(app: any): void {


      // aws.config.update({
      //   secretAccessKey:"Ge4u+o67ASc1S19AvlN82ZUlhuhe5svtNN3hM+BQ", 
      //   accessKeyId: "AKIA3A2OIKAXTCOQVIX4",
      //   region: "us-west-2"
      // });

      // const fileFilter = (req : any, file : any, cb: any) => {
      //   if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
      //     cb(null, true);
      //   } else {
      //     cb(new Error("Invalid format, only JPG and PNG"), false);
      //   }
      // };  
      // const s3 = new aws.S3();

      // const upload = multer({
         
      //   storage: multerS3({
      //     s3: s3, 
      //     bucket: "sikkimdev",
      //     acl: "public-read",
      //     metadata: function (req, file, cb) {
      //       cb(null, { fieldName: file.fieldname });
      //     },
      //     key: function (req, file, cb) {
            
      //       cb(null, Date.now().toString() + '.' + file.mimetype.split('/')[1]);
      //     },
      //   }),
      // });
 
      const storage = multer.diskStorage({
        destination: (req, file, cb) => {
          cb(null, './public/file');
        },
        filename: (req, file, cb) => { 
            console.log(file)
          cb(null, 'file-' + Date.now() + '.' + file.mimetype.split('/')[1]);
        }
    });

    const upload = multer({ storage: storage });

        /* #region  Hotels Route */
        app.route('/hotels/approved').get(HotelController.getApprovedHotels);
        
        app.route('/hotels').get(HotelController.getAllHotels);

        app.route('/hotels/:id').get(HotelController.getHotelById);
        app.route('/hotels/:id').delete(HotelController.deleteHotelById);

        
        app.route('/hotels/list/:ownerName').get(HotelController.getHotelsByOwner);

        app.route('/hotels/:id').put(HotelController.putHotel);

        app.route('/hotels').post(HotelController.postHotel);

       

        app.route('/hotels/search/:search').get(HotelController.searchHotelsByNameAndLocation);

        
        /* #endregion */

        /* #region  Destination Route */
        app.route('/destinations').get(DestinationController.getAllDestinations);

        app.route('/destinations').put(DestinationController.putDestination);

        app.route('/destinations').post(DestinationController.postDestination);
        
        app.route('/destinationForeigner').get(DestinationController.getAllDestinationsForeigner);

        app.route('/destinations/:id').get(DestinationController.getDestinationById);

        app.route('/destinations/permit-required/:isPermitRequire').get(DestinationController.getDestinationByIsPermitRequire);
        
        app.route('/destinations/ticket-booking/:isTicketBooking').get(DestinationController.getDestinationByTicketBooking);

        /* #endregion */

        /* #region  Treks Route */

        app.route('/treks/:id').get(TrekController.getTrekById);

        app.route('/treks/level/:level').get(TrekController.getAllTreks);

        /* #endregion */

        app.route('/travel-agent').get(TravelAgentController.getAllTravelAgents);
        app.route('/travel-agent/approved').get(TravelAgentController.getApprovedTravelAgents);
        app.route('/travel-agent').post(TravelAgentController.postTravelAgent);
        app.route('/travel-agent/:id').get(TravelAgentController.getTravelAgentById);
        app.route('/travel-agent/:id').put(TravelAgentController.putTravelAgent);
        app.route('/travel-agent/list/:agentName').get(TravelAgentController.getTravelagentByOwner);
        
        
        app.route('/experience/type/:type').get(ExperienceController.getAllExperience);
        
        app.route('/experience/:id').get(ExperienceController.getExperienceById);

        app.route('/feedback').post(FeedBackController.postFeedBack);

        app.route('/masters').get(MasterController.getAllMasterDetails);

        app.route('/payment').get(PaymentController.postPaymentDetails);

        app.route('/order').post(CartController.createOrderDetails);


        app.route('/order-bulk').put(CartController.bulkUpdateOrderDetails);
        

        app.route('/order/:type/:emailId').get(CartController.getOrderDetailsByTypeAndEmailId);

        app.route('/order/:id').delete(CartController.deleteCartDetails);

        // app.route('/user-details').post(UserController.postUser);

        app.route('/user-details/:type').post(UserController.postUser);

        app.route('/user-details/:id').put(UserController.putUser);

        app.route('/user-request-otp/:userType/:emailId').put(UserController.requestOtp);

        app.route('/user-reset-pwd/:id').put(UserController.resetPassword);

        
        
        app.route('/user-details/id/:id').get(UserController.getUserById);

        app.route('/user-details/:emailId').get(UserController.getUserByEmailId);

        app.route('/user-details/type/:type').get(UserController.getUserByUserType);

        app.route('/user-details?').get(UserController.getUserByEmailIdAndPwd);

        app.route('/permit').post(PermitController.postPermit);

        app.route('/permittype').post(PermitController.postPermittype);

        app.post('/file-upload', upload.single('file'), FileUploadController.fileUpload)
        
    }
}