import nodeMailer from 'nodemailer';
import IUser from './interface/User';
class Utility {
  constructor() {

  }

  public sendMail(user: IUser , subject: string, mailBody: string | undefined) {
    console.log('Send Mail')
    var transporter = nodeMailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'subramani.mohan91@gmail.com',
        pass: '9790480111'
      }
    });

    var mailOptions = {
      from: 'subramani.mohan91@gmail.com',
      to: user.emailId,
      subject: subject,
      html: mailBody
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  }

  public generateOTP() {
    const digits = '0123456789';
    const otpLength = 6;
    let otp = '';
    for (let i = 1; i <= otpLength; i++) {
      var index = Math.floor(Math.random() * (digits.length));
      otp = otp + digits[index];
    }
    return otp;
  }

  public generateTemporaryPassword() {
    const keys = {
      upperCase: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
      lowerCase: "abcdefghijklmnopqrstuvwxyz",
      number: "0123456789",
      symbol: "!@#$%^&*()_+~|}{[]:;?><,./-="
    }

    const getKey = [
      function upperCase() {
        return keys.upperCase[Math.floor(Math.random() * keys.upperCase.length)];
      },
      function lowerCase() {
        return keys.lowerCase[Math.floor(Math.random() * keys.lowerCase.length)];
      },
      function number() {
        return keys.number[Math.floor(Math.random() * keys.number.length)];
      },
      function symbol() {
        return keys.symbol[Math.floor(Math.random() * keys.symbol.length)];
      }
    ];

    const length = 8;
    let password = "";
    while (length > password.length) {
      let index = password.length;
      if(password.length > 3) {
        index = password.length - 4;
      }
      password += getKey[index];
    }
    return password;
  }
}

export default Utility;