import Hotel from '../models/Hotel';
import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose';
import HttpResponse from '../interface/HttpResponse';
import { ErrorResponse } from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
import IHotel from '../interface/Hotel';
import { Status } from '../config/CommonEnum';
import { CustomSuccessCode } from '../config/CustomeSuccessCode';

export const getAllHotels = async (req: Request, res: Response, next: NextFunction) => {

    try {


        const listOfHotels: any = await Hotel.find({ isActive: true } );

        next(new HttpResponse(HttpStatusCode.OK, true, listOfHotels));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export const getApprovedHotels = async (req: Request, res: Response, next: NextFunction) => {

    try {

        
        const listOfHotels: any = await Hotel.find({ isActive: true , status : 'Approved' }, { _id: 1, hotelName: 1, hotelAddress: 1, images: 1 , district: 1});

        next(new HttpResponse(HttpStatusCode.OK, true, listOfHotels));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export const getHotelsByOwner = async (req: Request, res: Response, next: NextFunction) => {

    try {
        const ownerName: any = req.params.ownerName;
        
        const listOfHotels: any = await Hotel.find({ hotelOwner : ownerName});

        next(new HttpResponse(HttpStatusCode.OK, true, listOfHotels));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export const searchHotelsByNameAndLocation = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const listOfHotels: any = await Hotel.find({
            $and: [{ isActive: true }],
            $or: [
                { hotelName: new RegExp(req.params.search, 'i') },
                { location: new RegExp(req.params.search, 'i') },
            ],
        }, { _id: 1, hotelName: 1, hotelAddress: 1, images: 1 , district: 1 })
        next(new HttpResponse(HttpStatusCode.OK, true, listOfHotels));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}


export const getHotelById = async (req: Request, res: Response, next: NextFunction) => {

    try {
        // 
        const id: any = req.params.id;

        if (!mongoose.Types.ObjectId.isValid(id)) {
            next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
            return;
        }

        const listOfHotels = await Hotel.findOne({ _id: id, isActive: true });
        next(new HttpResponse(HttpStatusCode.OK, true, listOfHotels));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export const deleteHotelById = async (req: Request, res: Response, next: NextFunction) => {

    try {
        // 
        const id: any = req.params.id;

        if (!mongoose.Types.ObjectId.isValid(id)) {
            next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
            return;
        }

        await Hotel.findByIdAndRemove(id);
        next(new HttpResponse(HttpStatusCode.OK, true, CustomSuccessCode.SK_SUC_0001));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}


export const putHotel = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const post = req.body;



        const hotel = await Hotel.findByIdAndUpdate(post._id, post, { new: true });
        next(new HttpResponse(HttpStatusCode.OK, true, hotel));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}


export const postHotel = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const post: IHotel = req.body;

        
        /* #region  Validate hotel is already exists or not */
        const isHotelExists = await Hotel.findOne({ hotelName: post.hotelName, isActive: true });
        if (isHotelExists) {
            next(new HttpResponse(HttpStatusCode.OK, false, null,CustomErrorCode.SK_ERR_0004, CustomErrorCode.SK_ERR_0004));
            return;
        }
        /* #endregion */
        //Set the status as initated on the creation
        post.status = Status.INITIATED;
        post.isActive = true;
        post.createdOn = new Date();
        const hotel = await Hotel.create(post);
        next(new HttpResponse(HttpStatusCode.OK, true, hotel));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export default {
    deleteHotelById, getAllHotels, getApprovedHotels,
    getHotelById, getHotelsByOwner,
    putHotel, postHotel,
    searchHotelsByNameAndLocation
};
