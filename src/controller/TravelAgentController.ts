
import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose'; 
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
import TravelAgents from '../models/TravelAgents'; 
import { Status } from '../config/CommonEnum';

export const getAllTravelAgents = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
         const listOfTravelAgents = await TravelAgents.find({isActive : true});
        next(new HttpResponse(HttpStatusCode.OK, true, listOfTravelAgents));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
export const getTravelAgentById = async (req: Request, res: Response, next: NextFunction) => {

    try {
        // 
        const id: any = req.params.id;

        if (!mongoose.Types.ObjectId.isValid(id)) {
            next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
            return;
        }

        const listOfTravelAgents = await TravelAgents.findOne({ _id: id, isActive: true });
        next(new HttpResponse(HttpStatusCode.OK, true, listOfTravelAgents));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export const postTravelAgent = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        const post = req.body;
        post.status = Status.INITIATED;
        post.isActive = true;
        post.createdOn = new Date();
        const travelagent = await TravelAgents.create(post);
        next(new HttpResponse(HttpStatusCode.OK, true, travelagent));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
export const getApprovedTravelAgents = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
         const listOfTravelAgents = await TravelAgents.find({isActive : true});
        next(new HttpResponse(HttpStatusCode.OK, true, listOfTravelAgents));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
export const putTravelAgent = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const post = req.body;



        const travelagent = await TravelAgents.findByIdAndUpdate(post._id, post, { new: true });
        next(new HttpResponse(HttpStatusCode.OK, true, travelagent));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}
export const getTravelagentByOwner = async (req: Request, res: Response, next: NextFunction) => {

    try {
        const ownerName: any = req.params.agentName;
        
        const listOfTravelAgents: any = await TravelAgents.find({ agentName : ownerName, isActive: true});

        next(new HttpResponse(HttpStatusCode.OK, true, listOfTravelAgents));
    }
    catch (error) {

        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

 
 
export default { getAllTravelAgents,postTravelAgent,putTravelAgent,getTravelAgentById,getApprovedTravelAgents,getTravelagentByOwner};
