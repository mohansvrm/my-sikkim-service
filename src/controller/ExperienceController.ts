
import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose'; 
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
import  Experience from '../models/Experience'; 

export const getAllExperience = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        
        const type : any = req.params.type;
         const listOfTravelAgents = await Experience.find({isActive : true, experienceType : type},{ _id : 1, title : 1, images : 1, contentType : 1 });
        next(new HttpResponse(HttpStatusCode.OK, true, listOfTravelAgents));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

export const getExperienceById = async (req : Request, res : Response, next : NextFunction) => {
    
    try {

        const id : any = req.params.id;
          
        if (!mongoose.Types.ObjectId.isValid(id)) {
            next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
            return;
        }

         const experience = await Experience.findOne({ _id : id, isActive : true});
        next(new HttpResponse(HttpStatusCode.OK, true, experience));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

 
 
export default { getAllExperience , getExperienceById };
