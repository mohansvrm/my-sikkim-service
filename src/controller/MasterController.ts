
import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose'; 
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
import Master from '../models/Master'; 

export const getAllMasterDetails = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
         const listOfMasters = await Master.find();
        next(new HttpResponse(HttpStatusCode.OK, true, listOfMasters));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
 
 
export default { getAllMasterDetails };
