 import FeedBack from '../models/FeedBack';  
import { NextFunction, Request, Response } from 'express';
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
 
 
export const postFeedBack = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        const post = req.body;
        const feedback = await FeedBack.create(post);
        next(new HttpResponse(HttpStatusCode.OK, true, feedback));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
 
export default {  postFeedBack };
