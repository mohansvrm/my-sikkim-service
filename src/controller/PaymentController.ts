 import Payment from '../models/Payment';  
import { NextFunction, Request, Response } from 'express';
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
import  Razorpay  from 'razorpay';
 
 
export const postPaymentDetails =  (req : Request, res : Response, next : NextFunction) => {
    
    try {
        const razorpay = new Razorpay({
            key_id: 'rzp_test_wS3m6RchQD9wqP',
            key_secret: 'iKXHyZcUEN2sjUo2SFkD2as0',
          });

          const options = {
            amount: 50000,  // amount in the smallest currency unit
            currency: "INR",
            receipt: "order_01"
          };
          razorpay.orders.create(options,(err : any,order : any)=>{
 
            if (order){
                
                
                const payment = {"userName":"Mohan Subramani",
                "mailId":"smohan.thi@gmail.com",
                "orderId":"",
                "payment":5000,"currency":"INR",
                "status":"Initiated",
                "paymentId" : "",
                "signatureId" : ""}
               const paymentdtls =  Payment.create(payment);
              next(new HttpResponse(HttpStatusCode.OK, true, payment));
                 
            }
        })
     
         
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
 
export default {  postPaymentDetails };
