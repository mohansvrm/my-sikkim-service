 import User from '../models/User';  
import { NextFunction, Request, Response } from 'express';
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
 
 
export const fileUpload = async (req : any, res : Response, next : NextFunction) => {
    try {
        next(new HttpResponse(HttpStatusCode.OK, true, req.file.location));
    } catch (err) {
        
        next(new HttpResponse(HttpStatusCode.BAD_REQUEST, true, err));
    }
}
 
export default {  fileUpload };
