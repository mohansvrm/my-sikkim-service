 import Permit from '../models/Permit';  
import { NextFunction, Request, Response } from 'express';
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
 
 
export const postPermit = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        const post = req.body;
         
           const permit = await Permit.insertMany(post);
        
           

        next(new HttpResponse(HttpStatusCode.OK, true, permit));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

export const postPermittype = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
       // const isPermittype : any =req.params.isPermittype;
        const post = req.body; 
        const result = await Permit.insertMany(post);
        next(new HttpResponse(HttpStatusCode.OK, true, result));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}



export default {  postPermit, postPermittype};
