
import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose'; 
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
import Destination from '../models/Destination'; 
import IDestination from '../interface/Destination';

export const getAllDestinations = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
         const listOfDestinations = await Destination.find({isActive : true, 
            isDestination : true});
        next(new HttpResponse(HttpStatusCode.OK, true, listOfDestinations));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

export const postDestination = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const post: IDestination = req.body;

        
        /* #region  Validate hotel is already exists or not */
        const isDestExists = await Destination.findOne({ hotelName: post.destName, isActive: true });
        if (isDestExists) {
            next(new HttpResponse(HttpStatusCode.OK, false, null,CustomErrorCode.SK_ERR_0004, CustomErrorCode.SK_ERR_0004));
            return;
        }
        /* #endregion */
        //Set the status as initated on the creation
        // post.status = Status.INITIATED;
        post.isActive = true;
        post.createdOn = new Date();
        const destination = await Destination.create(post);
        next(new HttpResponse(HttpStatusCode.OK, true, destination));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export const putDestination = async (req: Request, res: Response, next: NextFunction) => {

    try {
        
        const post = req.body;
        const destination = await Destination.findByIdAndUpdate(post._id, post, { new: true });
        next(new HttpResponse(HttpStatusCode.OK, true, destination));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001, CustomErrorCode.SK_ERR_0001));
    }
}

export const getAllDestinationsForeigner = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
         const listOfDestinations = await Destination.find({isActive : true, isForeigner: true},
            { _id : 1, destName : 1, address : 1,images : 1, district : 1 });
        next(new HttpResponse(HttpStatusCode.OK, true, listOfDestinations));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}


export const getDestinationById = async (req : Request, res : Response, next : NextFunction) => {
    
    try {

        const id : any = req.params.id;
          
        if (!mongoose.Types.ObjectId.isValid(id)) {
            next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
            return;
        }

         const destination = await Destination.findOne({ _id : id, isActive : true});
        next(new HttpResponse(HttpStatusCode.OK, true, destination));
    }
    catch (error) {
        
       
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}





export const getDestinationByTicketBooking = async (req : Request, res : Response, next : NextFunction) => {
    
    try {

        const isTicketBooking : any =req.params.isTicketBooking;

        
           
         const listOfHotels  : any = await Destination.find({isActive: true, ticketBooking : isTicketBooking});
        next(new HttpResponse(HttpStatusCode.OK, true, listOfHotels));
    }
    catch (error) { 
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}


export const getDestinationByIsPermitRequire = async (req : Request, res : Response, next : NextFunction) => {
    
    try {

        const isPermitRequire : any =req.params.isPermitRequire;

        
        
        const listOfDestinations  : any = await Destination.find({isActive: true, isPermitRequired : isPermitRequire});
        next(new HttpResponse(HttpStatusCode.OK, true, listOfDestinations));
    }
    catch (error) { 
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
 
 
export default {postDestination , putDestination, getAllDestinations, getDestinationById, getDestinationByTicketBooking,getDestinationByIsPermitRequire,getAllDestinationsForeigner};
