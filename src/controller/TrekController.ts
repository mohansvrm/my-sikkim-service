import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose'; 
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
import Trek from '../models/Trek';

export const getAllTreks = async (req : Request, res : Response, next : NextFunction) => {
    
    try {

        const level : any = req.params.level;
         const listOftreks = await Trek.find({ level : level, isActive : true},{ _id : 1, images : 1,level : 1,trkeName:1, contentType : 1 , description: 1});
        next(new HttpResponse(HttpStatusCode.OK, true, listOftreks));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}



export const getTrekById = async (req : Request, res : Response, next : NextFunction) => {
    
    try {

        const id : any = req.params.id;
          
        if (!mongoose.Types.ObjectId.isValid(id)) {
            next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
            return;
        }

         const trek = await Trek.findOne({ _id : id, isActive : true});
        next(new HttpResponse(HttpStatusCode.OK, true, trek));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

 
export default {getAllTreks, getTrekById };