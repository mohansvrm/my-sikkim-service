 import User from '../models/User';  
import { NextFunction, Request, Response } from 'express';
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode'; 
import Utility from '../utility';
import { oneTimePassword, temporaryPassword } from '../templates/TemporaryPassword';

export const postUser = async (req : Request, res : Response, next : NextFunction) => {
    try {
        const utility : Utility = new Utility();
        const {type : _type} = req.params;
        const post = req.body;
        let user;
        if(_type === 'Login') {
            user =await User.findOne({ emailId: post.emailId,userType : post.userType} );
        } else {
             user =await User.findOne({ 
                 $and : [{userType : post.userType}],
                $or: [
                    { emailId: post.emailId },
                    { phoneNumber: post.phoneNumber }
                  ]
                });
            if(user) {
                next(new HttpResponse(HttpStatusCode.OK, false, CustomErrorCode.SK_ERR_0002));
                return;
            }
        }
        if(!user) {
            if(post.userType === 'ADMIN') {
                post.isTempPwd = true;
                post.password = utility.generateTemporaryPassword();
            }
            
           user = await User.create(post);

           if(user.userType === 'ADMIN') {
            utility.sendMail(user,'Temporary Password',temporaryPassword(post.password));
           }
        }

        next(new HttpResponse(HttpStatusCode.OK, true, user));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

export const resetPassword = async (req : Request, res : Response, next : NextFunction) => {
    try {
            const post = req.body;
            const id = req.params.id;
            const utility : Utility = new Utility();
            post.password = utility.generateTemporaryPassword();
            post.isTempPwd = true;
            const user  = await User.findByIdAndUpdate(id,post,{new : true});
            utility.sendMail(post,'One Time Password',oneTimePassword(post.password));
            next(new HttpResponse(HttpStatusCode.OK, true, user));
         
    }
    catch (error) {
        next(new ErrorResponse(HttpStatusCode.INTERNAL_SERVER, CustomErrorCode.SK_ERR_0005,error));
    }
}

export const putUser = async (req : Request, res : Response, next : NextFunction) => {
    try {
        const post = req.body;
        const {id : _id} = req.params;
        const user = await User.findByIdAndUpdate(_id,post,{new : true});
        next(new HttpResponse(HttpStatusCode.OK, true, user));
    }
    catch (error) {
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

export const requestOtp = async (req : Request, res : Response, next : NextFunction) => {
    try {
        const post = req.body;
        const emailId = req.params.emailId;
        const userType = req.params.userType;
        const user = await User.findOne({emailId : emailId,userType : userType});
        if(user) {
            if(user.gAuth) {
                next(new HttpResponse(HttpStatusCode.OK, false, CustomErrorCode.SK_ERR_0006));
                return;
            }
            const utility : Utility = new Utility();
            user.otp = utility.generateOTP();
            const user_tmp = await User.findByIdAndUpdate(user._id,user,{new : true});
            utility.sendMail(user,'One Time Password',oneTimePassword(user.otp));

        next(new HttpResponse(HttpStatusCode.OK, true, user_tmp));
        } else {
            next(new HttpResponse(HttpStatusCode.OK, false, CustomErrorCode.SK_ERR_0001));
        }
       
    }
    catch (error) {
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
 
export const getUserByEmailId = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        const emailId : any = req.params.emailId;
        let user =await User.findOne({emailId : emailId});
        next(new HttpResponse(HttpStatusCode.OK, true, user));
    }
    catch (error) {
        next(new ErrorResponse(HttpStatusCode.INTERNAL_SERVER, CustomErrorCode.SK_ERR_0005,error));
    }
}

export const getUserById = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        const id : any = req.params.id;
        let user =await User.findOne({_id : id});
        next(new HttpResponse(HttpStatusCode.OK, true, user));
    }
    catch (error) {
        next(new ErrorResponse(HttpStatusCode.INTERNAL_SERVER, CustomErrorCode.SK_ERR_0005,error));
    }
}

export const getUserByUserType = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        const userType : any = req.params.type;
        let users =await User.find({userType : userType});
        next(new HttpResponse(HttpStatusCode.OK, true, users));
    }
    catch (error) {
        next(new ErrorResponse(HttpStatusCode.INTERNAL_SERVER, CustomErrorCode.SK_ERR_0005,error));
    }
}

export const getUserByEmailIdAndPwd = async (req : Request, res : Response, next : NextFunction) => {
    
    try {
        console.log('Query ',req.query);
        let user =await User.findOne(req.query);
        
        if(user) {
            next(new HttpResponse(HttpStatusCode.OK, true, user));
        } else {
            next(new HttpResponse(HttpStatusCode.OK, false,CustomErrorCode.SK_ERR_0003));
        }
        
    }
    catch (error : any) {
        next(new ErrorResponse(HttpStatusCode.INTERNAL_SERVER, CustomErrorCode.SK_ERR_0005,error));
    }
}
 
export default {  postUser, getUserByEmailId, 
    putUser, getUserByEmailIdAndPwd,
    getUserByUserType, getUserById, requestOtp,
    resetPassword };
