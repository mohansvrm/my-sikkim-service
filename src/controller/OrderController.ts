 import Order from '../models/Order';  
import { NextFunction, Request, Response } from 'express';
import HttpResponse from '../interface/HttpResponse';
import {ErrorResponse} from '../interface/ErrorResponse';
import { HttpStatusCode } from '../config/HttpStatusCode';
import { CustomErrorCode } from '../config/CustomErrorCode';
 
 
export const createOrderDetails = async (req : Request, res : Response, next : NextFunction) => {
    try {
       
        const post = req.body;
        const order = await Order.create(post);
        next(new HttpResponse(HttpStatusCode.OK, true, order));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

export const bulkUpdateOrderDetails = async (req : Request, res : Response, next : NextFunction) => {
    try {
       
        const listOfOrders = req.body;
        if(listOfOrders) {
            const listOfOrderId = listOfOrders.map((order : any) =>   order._id);
            
            var bulk = Order.collection.initializeOrderedBulkOp();
            bulk.find({'_id': {$in: [
                '613c483cc2836187b4a40895',
                '613c48e2c2836187b4a4089d',
                '613c56f9871d8d7a1076cfc6',
                '614d59de5bae3c4cd4859b95'
              ]}}).update({$set: {
            
                type : 'Order'
                }});

                bulk.execute(function (error) {
                    
               });
            // await bulk.execute();
            next(new HttpResponse(HttpStatusCode.OK, true, listOfOrders));
        } else {

        }
        // const order = await Order.create(post);
        next(new HttpResponse(HttpStatusCode.OK, false, []));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
export const getOrderDetailsByTypeAndEmailId = async (req : Request, res : Response, next : NextFunction) => {
    try {
        const emailId : any = req.params.emailId;
        const type : any = req.params.type;
        const listOfCart = await Order.find({type : type,userName : emailId});
        next(new HttpResponse(HttpStatusCode.OK, true, listOfCart));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}

export const deleteCartDetails = async (req : Request, res : Response, next : NextFunction) => {
    try {
        const emailId : any = req.params.id;
        await Order.deleteOne({_id : emailId});
        next(new HttpResponse(HttpStatusCode.OK, true, "Success"));
    }
    catch (error) {
        
        next(new ErrorResponse(HttpStatusCode.NOT_FOUND, CustomErrorCode.SK_ERR_0001,CustomErrorCode.SK_ERR_0001));
    }
}
 
export default { bulkUpdateOrderDetails, createOrderDetails, getOrderDetailsByTypeAndEmailId, deleteCartDetails };
