export const temporaryPassword = (password : string) => {
    return `<b>${password}</b>`;
}


export const oneTimePassword = (password : string) => {
    return `One Time Password<b>${password}</b>`;
}

export default {  temporaryPassword, oneTimePassword };