import { Buffer } from 'buffer';


export default interface IImage  {
    image : Buffer;
    contentType : string;
}