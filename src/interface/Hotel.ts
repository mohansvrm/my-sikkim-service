import { Document } from 'mongoose';

export default interface IHotel extends Document {
    hotelName: string;
    hotelAddress: string;
    district: string;
    images:Array<IHotelImages>;
    documents:Array<IHotelDocuments>;
    rating: string;
    contentType : string;
    overview: string;
    hotelOwner: string;
    about: string;
    phoneNoOne : string;
    phoneNoTwo: string;
    mailIdOne: string;
    mailIdTwo: string;
    isActive: boolean;
    status: string;
    amenities : Array<IAmenieties>;
    createdOn : Date;
    // roomType : Array<IRoomType>;
    // reviews : Array<IReview>;
}

export interface IHotelImages {
    isMainImage: boolean;
    path: string;
}

export interface IHotelDocuments {
    documentName: string;
    path: string;
}

export interface IAmenieties {
    code : string;
    value : string;
    image: string;
    checked : boolean;
}

 
     