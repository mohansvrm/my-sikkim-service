import { Document } from 'mongoose';

export default interface ITravelAgent extends Document {
    _id: string;
    address: string;
    agentName: string;
    grade: string;
    mailId: string;
    phoneNumber: string;
    status: string;
    amenities : Array<IAmenieties>; 
    createdOn : Date;
} 
export interface IFacility {
    name : string;
    icon: string;
}

export interface IAmenieties {
    code : string;
    value : string;
    image: string;
    checked : boolean;
}
 