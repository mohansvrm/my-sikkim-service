import { Document } from 'mongoose';
import IImage from './Image';

export default interface IPermit extends Document {

    locationName: string;
    documentType: string;
    documentNumber: string;
    photo: IImage;
    permittype:string
}


