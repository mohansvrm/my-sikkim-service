import { Document } from 'mongoose';

export default interface IExperience extends Document {
    experienceType: string;
    isActive: boolean;
    images: Array<IExperienceimage>
    contentType : string;
    about : string;
    title : string;
}
export interface IExperienceimage {
    isMainImage: boolean;
    path: string;
}