import { Document } from 'mongoose';

export default interface IFeedBack extends Document {
    name: string;
    mailId: string;
    rating: number;
    reviewComment: string;
}
