import { Document } from 'mongoose';
import { IHotelImages } from './Hotel';

// export default interface IDestination extends Document {
//     destName: string;
//     location: string;
//     ticketBooking: boolean;
//     isActive: boolean;
//     isForeigner: boolean;
//     image: string;
//     contentType : string;
//     about : string;
//     childrenFee : number;
//     adultFee : number;
// }


export default interface IDestination extends Document {
    destName: string;
    address: string;
    district: string;
    isForeigner?: boolean ;
    about?: string;
    images : IHotelImages [];
    ticketBooking?: boolean ;
    isPermitRequired?: boolean ;
    permitFee : number;
    childrenFee: number;
    adultFee: number;
    isActive?: boolean ;
    isDestination?: boolean;
    createdBy : string;
    createdOn : Date | undefined;
    modifiedBy : string;
    modifiedOn : Date | undefined;
}