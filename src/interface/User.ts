import { Document } from 'mongoose';
import IImage from './Image';

export default interface IUser extends Document {
    password: string;
    otp: string;
    
    userName: string;
    fullName: string;
    phoneNumber: string;
    emailId: string;
    address: string;
    idproof: string;
    vaccine: string;
    profile: string
    userType : string;

    gAuth: boolean;
}


