import { Document } from 'mongoose'; 

export default interface IOrder extends Document {
    name: string;
    adultCount: number;
    adultFee: number;
    childrenCount: number;
    childrenFee: number;
    orderDate: string;
    userName: string; 
    type: string;
    orderId: string;
    locationName : string ;
}


  