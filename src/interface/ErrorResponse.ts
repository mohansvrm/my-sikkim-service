export class ErrorResponse{
    public errorCode : string = '';
    public errorMessage : string = '';
    public httpStatusCode : number = 500;

    constructor(httpStatusCode : number = 500,
                errorCode : string = '',
                errorMessage : any = ''){
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.httpStatusCode = httpStatusCode;

    }
}
// export = ErrorResponse;