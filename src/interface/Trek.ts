import { Document } from 'mongoose';

export default interface ITrek extends Document {
    trekName: string;
    description: string;
    image: string;
    level: string;
    isActive : boolean;
    days : string;
    minAge : string;
}
