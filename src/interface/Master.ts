import { Document } from 'mongoose';

export default interface IMaster extends Document {
    _id: string;
    lookUpName: string;
    lookupValues : Array<ILookupValues>; 
} 
export interface ILookupValues {
    code : string;
    value: string;
}

 