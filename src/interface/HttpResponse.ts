import {ErrorResponse} from "./ErrorResponse";

class HttpResponse extends ErrorResponse {
    public isSuccess : boolean  = false;
    public data : any = null;

    constructor(httpStatusCode : number = 500,isSuccess:boolean = false,data: any = null,
        errorCode : string = '', errorMessage : string = ''
                ){
        super(httpStatusCode ,errorCode,errorMessage);
        this.isSuccess = isSuccess;
        this.data = data;
       

    }
}
export = HttpResponse;