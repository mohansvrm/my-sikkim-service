import { Document } from 'mongoose';

export default interface IPayment extends Document {
    userName: string;
    mailId: string;
    orderId: string;
    payment: number;
    currency : boolean;
    status : string; 
}
