 import mongoose , { Schema } from "mongoose";
 import ITravelAgent from "../interface/TravelAgent";

 
const TravelAgentSchema = new Schema<ITravelAgent>({
    code :{ type: String, required: false },
    address:{ type: String, required: false },
    agentName: { type: String, required:  false  },
    grade: { type: String, required:  false },
     
    mailId: { type: String, required: false },
    phoneNumber: { type: String, required: false },
    isActive : { type: Boolean, required: false },
    status : { type: String, required: false },
    amenities: [{
        code : { type: String, required: false },
        value : { type: String, required: false },
        image: { type: String, required: false },
        checked: { type: Boolean, required: false }
    
    }],
    createdOn: { type: Date},
    updatedOn: { type: Date, default: Date.now }
    
});
export default mongoose.model<ITravelAgent>('TravelAgent', TravelAgentSchema); 
 