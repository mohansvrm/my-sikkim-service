import mongoose , { Schema } from "mongoose";
import IDestination from "../interface/Destination";


const DestinationSchema = new Schema<IDestination>({
    code:{ type: String, required: true },
    destName:{ type: String, required: true },
    address: { type: String, required: true },
   district: { type: String, required: true },
   isForeigner: { type: Boolean, required: true },
   about: { type: String, required: true },
   images:
   [{
       isMainImage: { type: Boolean, required: true },
       path: { type: String, required: true }
   }],
   ticketBooking: { type: Boolean, required: true },
   childrenFee : { type: Number, required: true },
   adultFee : { type: Number, required: true },
   isPermitRequired : { type: Boolean, required: true },
   permitFee : { type: Number, required: true },
   isActive: { type: Boolean, required: true },
   isDestination: { type: Boolean, required: true },
   createdOn: { type: Date},
   updatedOn: { type: Date, default: Date.now },
   createdBy:{ type: String, required: true },
   modifiedBy: { type: String, required: false }
});
export default mongoose.model<IDestination>('Destination', DestinationSchema); 

 
  