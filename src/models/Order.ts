import mongoose , { Schema } from "mongoose";
import IOrder from "../interface/Order";


const OrderSchema = new Schema<IOrder>({

    name:{ type: String, required: true },
    type:{ type: String, required: true },
    adultCount: { type: Number, required: true },
    adultFee: { type: Number, required: true },
    childrenCount: { type: Number, required: true },
    childrenFee: { type: Number, required: true },
    orderDate:{ type: String, required: true },
    locationName:{ type: String, required: true },
    userName:{ type: String, required: true },
    orderId: { type : String , required: false},
    razorPaymentId: { type : String , required: false}
});
export default mongoose.model<IOrder>('Order', OrderSchema); 
  