import mongoose , { Schema } from "mongoose";
import IUser from "../interface/User";


const UserSchema = new Schema<IUser>({

    userName:{ type: String, required: true },
    fullName: { type: String, required: false },
    phoneNumber:{ type: String, required: false },
    emailId: { type: String, required: true },
    address: { type: String, required: false },
    idproof:{ type: String, required: false },
    vaccine:{ type: String, required: false },
    profile:{ type: String, required: false },
    password:{ type: String, required: false },
    userType :{ type: String, required: false },
    role:{ type: String, required: false },
    otp:{ type: String, required: false },
    gAuth:{ type: Boolean, required: false },
    isTempPwd:{ type: Boolean, required: false },
    isActive:{ type: Boolean, required: false },
    createdOn: { type: Date},
    updatedOn: { type: Date, default: Date.now }
    
//     aadhar:
//     {
//        image: { type: Buffer, required: false },
//        contentType: { type: String, required: false }
//    } ,
//    vaccine:
//    {
//       image: { type: Buffer, required: false },
//       contentType: { type: String, required: false }
//   } ,
//   profile:
//    {
//       image: { type: Buffer, required: false },
//       contentType: { type: String, required: false }
//   }
  
});
export default mongoose.model<IUser>('User', UserSchema); 
 