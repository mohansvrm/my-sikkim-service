 import mongoose , { Schema } from "mongoose";
 import IMaster from "../interface/Master";  

 
const MasterSchema = new Schema<IMaster>({

    _id: { type: String, required: true },
    lookUpName:{ type: String, required: true } ,
    lookupValues: [{
        code : { type: String, required: true },
        value : { type: String, required: true } 
    }],
    
});
export default mongoose.model<IMaster>('Master', MasterSchema); 
 
 