import mongoose , { Schema } from "mongoose";
import IPermit from "../interface/Permit";


const PermitSchema = new Schema<IPermit>({

  locationName:{ type: String, required: false },
  documentType: { type: String, required: true },
  documentNumber:{ type: String, required: false },
  photo:
   {
      image: { type: Buffer, required: false },
      contentType: { type: String, required: false }
  },
  permitType:{ type: String, required: false }
  
});
export default mongoose.model<IPermit>('Permit', PermitSchema); 
 