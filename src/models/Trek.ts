import mongoose , { Schema } from "mongoose";
import ITrek from "../interface/Trek";


const TrekSchema = new Schema<ITrek>({

   trekName:{ type: String, required: true },
   description: { type: String, required: true },
   images:
   [{
       data: { type: String, required: true },
       contentType: { type: String, required: true }
   }],
   image: { type: String, required: true },
   level: { type: String, required: true },
   isActive: { type: Boolean, required: true },
   days : { type: String, required: true },
   minAge : { type: String, required: true }
});
export default mongoose.model<ITrek>('Trek', TrekSchema); 