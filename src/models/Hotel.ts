 import mongoose , { Schema } from "mongoose";
 import IHotel from "../interface/Hotel";

 
const HotelSchema = new Schema<IHotel>({

    
    hotelName:{ type: String, required: true },
    hotelAddress: { type: String, required: true },
    district: { type: String, required: true },
    images:
    [
        {
            isMainImage : { type: Boolean, required: false },
            path : { type: String, required: false }
        }
        
    ],
    documents:
    [
        {
            documentName : { type: String, required: false },
            path : { type: String, required: false }
        }
        
    ],
    overview: { type: String, required: false },
    hotelOwner: { type: String, required: false },
    about: { type: String, required: false },
    rating: { type: String, required: false },
    phoneNoOne: { type: String, required: false },
    phoneNoTwo: { type: String, required: false },
    mailIdOne: { type: String, required: false },
    mailIdTwo: { type: String, required: false },
    isActive : { type: Boolean, required: false },
    status : { type: String, required: false },
    amenities: [{
        code : { type: String, required: false },
        value : { type: String, required: false },
        image: { type: String, required: false },
        checked: { type: Boolean, required: false }
    
    }],
    comments : [
        {
            comment : { type: String, required: false },
            time : { type: Date, required: false } 
        }
    ],
    createdOn: { type: Date},
    updatedOn: { type: Date, default: Date.now }
});
export default mongoose.model<IHotel>('Hotel', HotelSchema); 

  