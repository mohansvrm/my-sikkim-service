import mongoose , { Schema } from "mongoose";
import IPayment from "../interface/Payment";


const PaymentSchema = new Schema<IPayment>({

    userName:{ type: String, required: true },
    mailId: { type: String, required: true },
    orderId: { type: String, required: true },
    payment: { type: Number, required: true },
    currency : { type: String, required: true },
    status : { type: String, required: true }
});
export default mongoose.model<IPayment>('Payment', PaymentSchema); 