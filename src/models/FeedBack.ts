import mongoose , { Schema } from "mongoose";
import IFeedBack from "../interface/FeedBack";


const FeedBackSchema = new Schema<IFeedBack>({

    name:{ type: String, required: true },
    mailId: { type: String, required: true },
    reviewComment: { type: String, required: true },
    rating: { type: Number, required: true }
    
});
export default mongoose.model<IFeedBack>('FeedBack', FeedBackSchema);