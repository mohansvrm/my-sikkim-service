import mongoose , { Schema } from "mongoose";
import IExperience from "../interface/Experience";


const ExperienceSchema = new Schema<IExperience>({

   experienceType:{ type: String, required: true }, 
   isActive: { type: Boolean, required: true },
   image: { type: Buffer, required: true },
   contentType: { type: String, required: true },
   about: { type: String, required: true },
   images:
    [
        {
            isMainImage : { type: Boolean, required: false },
            path : { type: String, required: false }
        }
        
    ],
   title: { type: String, required: true }
   
});
export default mongoose.model<IExperience>('Experience', ExperienceSchema); 