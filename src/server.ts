import app from './app';
import config from './config/Config';
// import https from 'https';
// import path from 'path';
// import fs from 'fs'; 
import Logger from './logger';
 
//If port number is not defined then exit
if(!process.env.PORT) {
    process.exit();
} 
console.log(`started`);
const PORT : number = parseInt(config.server.port as string, 10);
app.listen(PORT, () => {
     console.log(`Server is up and running on PORT ${PORT}`);
    
})


// const sslServer = https.createServer({ 
//     key : fs.readFileSync(path.join(__dirname, 'cert', 'key.pem')),
//     cert : fs.readFileSync(path.join(__dirname, 'cert', 'cert.pem'))
//     },app);    

// sslServer.listen(PORT, () => { 
//         
//     })