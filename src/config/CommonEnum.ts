export enum Status {
    INITIATED = "Initiated",
    APPROVED = "Approved",
    REJECTED = "Rejected"
}