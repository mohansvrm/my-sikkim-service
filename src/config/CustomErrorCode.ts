export enum CustomErrorCode {
    SK_ERR_0001 = 'Resource does not exists',
    SK_ERR_0002 = 'User already exists',
    SK_ERR_0003 = 'User name or Password mismatch',
    SK_ERR_0004 = 'Hotel Name is already exists' ,
    SK_ERR_0005 = 'Internal Server Error',
    SK_ERR_0006 = 'Please login with google'
}