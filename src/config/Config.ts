import dotenv from 'dotenv';
import {resolve} from 'path';
// dotenv.config();

if(process.env.NODE_ENV?.includes('production')) {
    dotenv.config({
        path: resolve(__dirname, "../../.env.production")
      });
} else if(process.env.NODE_ENV?.includes('development')) {
    dotenv.config({
        path: resolve(__dirname, "../../.env.development")
      });
}



// dotenv.config({ path: '../../.env.'+process.env.NODE_ENV });

// 

// require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

const MONGO_OPTIONS = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    socketTimeoutMS: 30000,
    keepAlive: true,
    poolSize: 50,
    autoIndex: false,
    retryWrites: false
};

// const MONGO_USERNAME = process.env.MONGO_USERNAME || 'superuser';
// const MONGO_PASSWORD = process.env.MONGO_USERNAME || 'supersecretpassword1';
// const MONGO_HOST = process.env.MONGO_URL || `ds343895.mlab.com:43895/mongobongo`;

const MONGO = {
    // host: MONGO_HOST,
    // password: MONGO_PASSWORD,
    // username: MONGO_USERNAME,
    options: MONGO_OPTIONS,
    url: "mongodb+srv://tourism:tourism@tourism.8lblr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"//`mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}`
};

const SERVER_HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
const SERVER_PORT = process.env.PORT || 1337;

const SERVER = {
    hostname: SERVER_HOSTNAME,
    port: SERVER_PORT
};

const SES_CONFIG = {
    
    accessKeyId: process.env.ACCESS_KEY,
    accessSecretKey: process.env.AWS_SECRET_KEY,
    region: process.env.BUCKET_REGION
} 

const config = {
    mongo: MONGO,
    server: SERVER,
    sesConfig : SES_CONFIG
};

export default config;